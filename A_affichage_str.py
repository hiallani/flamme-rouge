######### SECTION : AFFICHAGE DU JEU ##########
##############################################

#Fonction ligne_vers_droite
#Pour représenter une ligne de route, où les coureurs se déplacent vers la droite
#positions est une liste contenant des caractères à afficher
def ligne_vers_droite(positions):
    l=">"
    for coureur in positions:
        l+=" "+coureur +" | "
    return l
#print(ligne_vers_droite(["A","A","B","C","D"]))
#Fonction route_vers_droite
#Une route est constituée de deux lignes en parallèle, pour représenter les deux voies.
#Chaque case est constituée de deux côtés, droite et gauche.
#positions_droite et positions_gauche sont des listes de même longueur contenant des caractères
def route_vers_droite(positions_gauche,positions_droite):
    if len(positions_droite)==len(positions_gauche):
        l1=ligne_vers_droite(positions_droite)
        l2=ligne_vers_droite(positions_gauche)
        return l2+"\n"+l1
#print(route_vers_droite(["A","A","B","C","D"],["A","A","B","C","G"]))

#Fonction inverse
#On souhaite maintenant créer des routes, où les coureurs se dirigent vers la gauche
#Cette fonction inverse une ligne, en enlevant le dernier élément et le remplaçant par "<"
def inverse(ligne):
    return ligne[:0:-1] + "<"
#Fonction route_vers_gauche
#Cette fonction renvoie une route où les coureurs se dirigent vers la gauche.
def route_vers_gauche(positions_droite,positions_gauche):
    l1=ligne_vers_droite(positions_droite)
    l2=ligne_vers_droite(positions_gauche)
    return inverse(l1)+"\n"+inverse(l2)
#print(route_vers_gauche(["A","A","B","C","D"],["A","A","B","C","G"]))
#Fonction ligne_depart
#Les routes de départ sont forcément vers la droite par défaut.
#Elles commencent par ">>>>>" au lieu de ">"
#Une zone de start est présente au début et termine avec un "X" au lieu de "|"
def ligne_depart(positions, start):
    ligne = ">>>>> "
    i=0
    while i<len(positions) and i<(start-1):
        ligne=ligne+positions[i]+" | "
        i+=1
    ligne+=positions[i]+" X "
    for i in range(start,len(positions)):
        ligne+=positions[i]+" | "
    return ligne 
#Fonction route_depart
#Cette fonction représente une route de départ avec deux lignes de départ en parallèle
def route_depart(positions_droite, positions_gauche, start):
    line_droite = ligne_depart(positions_droite, start)
    line_gauche = ligne_depart(positions_gauche, start)
    return line_gauche + "\n" + line_droite
#Fonction ligne_arrivee_vers_droite
#On représente désormais une ligne d'arrivée où les coureurs se dirigent vers la droite
#Un entier terminus > 1 indique l'emplacement de la ligne d'arrivee pour gagner la course
def ligne_arrivee_vers_droite(positions,terminus):
    line = "> "
    i=0
    while i<(len(positions)) and i<(terminus-1):
        line+=positions[i] + " | "
        i+=1
    if (i<len(positions)):
        line+=positions[i]+" |XXXX| "
    for i in range(terminus,len(positions)):
        line+=positions[i] + " | "
    return line

#Fonction route_arrivee_vers_droite
##Cette fonction représente une route de départ avec
#deux lignes d'arrivee en parallèle
def route_arrivee_vers_droite(positions_droite,positions_gauche,terminus):
    ld=ligne_arrivee_vers_droite(positions_droite,terminus)
    lg=ligne_arrivee_vers_droite(positions_gauche,terminus)
    return lg + "\n" + ld
#Fonction route_arrivee_vers_gauche
#Même chose que route_arrivee_vers_droite sauf que cette fois_ci
#les coureurs se déplacent vers la gauche
def route_arrivee_vers_gauche(positions_droite,positions_gauche,terminus):
    ld=inverse(ligne_arrivee_vers_droite(positions_droite,terminus))
    lg=inverse(ligne_arrivee_vers_droite(positions_gauche,terminus))
    return ld + "\n" +lg
#Fonction affichage_route
#Etant donné un nombre de lignes, la longueur de chaque ligne (taille_seg),
#deux liste de positions (pour la droite et la gauche),
#on affiche la route avec les caractéristiques indiquées et le contenu des listes
#de positions. L'emplacement du départ sur la première ligne (start) et de l'arrivée
#sur la dernière (terminus) sont donnés comme arguments optionnels
#On suppose nb_lignes >= 2
def affichage_route(nb_lignes,taille_seg,positions_droite,positions_gauche,start=4,terminus=10):
    j=0
    taille=0
    positions_d=positions_droite[:taille_seg]
    positions_g=positions_gauche[:taille_seg]
    route=route_depart(positions_d,positions_g,start)+"\n"
    i=2
    j+=taille_seg
    taille=taille_seg +j
    while i<=(nb_lignes -1):
        positions_d=positions_droite[j:taille]
        positions_g=positions_gauche[j:taille]
        if i%2==0:
            route+=route_vers_gauche(positions_d,positions_g)+"\n"
        else:
            route+=route_vers_droite(positions_d,positions_g)+"\n"
        i+=1
        j+=taille_seg
        taille=taille_seg+j
    positions_d=positions_droite[j:taille]
    positions_g=positions_gauche[j:taille]
    if i%2==0:
        route+=route_arrivee_vers_gauche(positions_d,positions_g,terminus)
    else:
        route+=route_arrivee_vers_droite(positions_d,positions_g,terminus)
    return route
