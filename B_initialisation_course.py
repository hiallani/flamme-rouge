from random import *

######### SECTION : INITIALISATION DE LA COURSE ##########
##########################################################

def creer_liste_joueurs(nb_joueurs):
    liste_des_coureurs=[]
    for i in range(nb_joueurs):
        liste_des_coureurs.append("J"+str(i))
    return liste_des_coureurs
    
def creer_listes_positions(nb_lignes,taille_seg):
    n=nb_lignes * taille_seg
    l1=["__"]*n
    l2=["__"]*n
    return l1,l2
    
def placer_joueur_alea(position_dispos):
    pos_choisie=choice(position_dispos)
    position_dispos.remove(pos_choisie)
    return pos_choisie
    
def initialiser_jeu_alea(liste_joueurs,positions_droite,positions_gauche,start):
    positions_dispos=[]
    n=len(positions_droite)
    for i in range(start):
        positions_dispos.append("D"+str(i))
    for j in range(start):
        positions_dispos.append("G"+str(j))
    for joueur in liste_joueurs:
        if len(positions_dispos)>0:
            pos=placer_joueur_alea(positions_dispos)
            ind=int(pos[1:])
            if pos[0]=="D":
                positions_droite[ind]=joueur
            elif pos[0]=="G" in pos:
                positions_gauche[ind]=joueur
                
def construit_dico_positions(positions_droite,positions_gauche,start):
    d={}
    for i in range (start):
        if positions_droite[i]!="__":
            d[positions_droite[i]]="D"+str(i)
        if positions_gauche[i]!="__":
            d[positions_gauche[i]]="G"+str(i)
    return d
    
def numero_de_case(nom_joueur,dico_positions):
    return int(dico_positions[nom_joueur][1:])
    
   


