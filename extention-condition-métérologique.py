import random*

# Conditions météorologiques possibles
conditions_meteo = ['Ensoleillé', 'Pluvieux', 'Venté', 'Brumeux', 'Neigeux']

# Effets des conditions météorologiques sur les déplacements
effets_meteo = {
    'Ensoleillé': 0,
    'Pluvieux': -1,
    'Venté': random.choice([-1, 1]),  # -1 pour vent de face, +1 pour vent de dos
    'Brumeux': None,  # Logique spéciale nécessaire pour les conditions brumeuses
    'Neigeux': -1  # Nécessite une carte de fatigue supplémentaire
}

# Types de cartes de compétence
cartes_competence = {
    'Sprint': 2,
    'Esquive': 'Annule l’effet négatif',
    'Aspiration': 1,
    'Endurance': 'Pas de carte de fatigue ce tour-ci',
    'Attaque d’Équipe': 2  # Logique additionnelle pour le jeu coopératif
}

# Choix de la météo
def choisir_meteo():
    return random.choice(conditions_meteo)

# Application des effets de la météo
def appliquer_effet_meteo(meteo, position_joueur):
    effet = effets_meteo[meteo]
    if meteo == 'Brumeux':
        print("La visibilité est réduite. Les joueurs ne peuvent voir que les joueurs proches.")
    elif meteo == 'Venté' and effet == -1:
        print("Vent de face! Tous les déplacements sont réduits de 1.")
    elif meteo == 'Venté' and effet == 1:
        print("Vent de dos! Tous les déplacements sont augmentés de 1.")
    else:
        position_joueur += effet
    return position_joueur

# Utilisation d'une carte de compétence
def utiliser_carte_competence(carte, position_joueur):
    if carte == 'Esquive':
        print("Les effets négatifs sont annulés pour ce tour.")
    elif carte in ['Sprint', 'Aspiration', 'Attaque d’Équipe']:
        position_joueur += cartes_competence[carte]
    elif carte == 'Endurance':
        print("Aucune carte de fatigue ne sera tirée ce tour-ci.")
    return position_joueur


