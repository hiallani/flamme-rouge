from random import *

######### SECTION : GESTION DES CARTES #################
########################################################

#Cette fonction donne le paquet initial détenu par chaque joueur
def paquet_initial():
    # ce paquet est initialement prévu pour une partie à 5 lignes,
    # des segments de 20 cases, un start = 4 et terminus = 10
    return [2] * 3 + [3] * 3 + [4] * 3 + [5] * 3 + [6] * 3 + [9] * 3
#Il va falloir stocker tout au long de la partie le paquet de cartes de chaque joueur
#Pour cela, on va manipuler un dictionnaire qui admettra les noms des joueurs
#comme clés et leur paquet comme valeur.
#Au début de la partie, les joueurs auront des paquets équivalents, mais ceux-ci évolueront
#au fur et à mesure de la partie, en fonction des déplacements de chacun.
#Cette fonction prend en entrée la liste des joueurs et renvoie le dictionnaire.
def dico_paquets_initial(liste_joueurs):
    dico = {}
    for joueur in liste_joueurs:
        dico[joueur]=paquet_initial()
    return dico
#Chaque joueur possède aussi une défausse contenant les cartes déplacements
#qu'il n'a pas choisi. Lorsque la défausse sera vide, on la mélange et remet son contenu
#dans le paquet. Au début de la partie, la défausse est vide.
#Cette fonction crée donc un dictionnaire où les clés sont les joueurs et les valeurs
#sont les défausses initiales (à savoir des listes vides)
def dico_defausses_initial(liste_joueurs):
    dico = {}
    for joueur in liste_joueurs:
        dico[joueur] = []
    return dico
def configurations_joueurs(liste_joueurs):
    dico_config = {}
    for joueur in liste_joueurs :
        mod=input("Quel mode de jeu pour le joueur {}:Aleatoire ou Manuel ?(A/M) ".format(joueur))
        while mod not in "aAMm":
            mod=input("Recommencez!Quel mode de jeu pour le joueur {}:Aleatoire ou Manuel ?(A/M) ".format(joueur))
        if mod == "a" or mod == "A":
            dico_config[joueur]= "A"
        elif mod=="m" or mod == "M":
            dico_config[joueur]= "M"
            
    print("Choix des configurations effectué !")
    return dico_config
def choix_carte_alea(main_joueur):
    ls=list(main_joueur)
    return  choice(ls)
def choix_carte_manuel(nom_joueur,main_joueur):
    x=int(input("Quel déplacement pour {} souhaitez-vous effectuer? {} ".format(nom_joueur,main_joueur)))
    while x not in main_joueur:
        x=int(input("Recommencez !Quel déplacement pour {} souhaitez-vous effectuer? {} ".format(nom_joueur,main_joueur)))
    return  x
def pioche_main_joueur(nom_joueur,dico_paquets,dico_defausses):
    main=[]
    if len(dico_paquets[nom_joueur])>=4:
        main=dico_paquets[nom_joueur][:4]
        dico_paquets[nom_joueur]=dico_paquets[nom_joueur][4:]
    else:
        main=dico_paquets[nom_joueur]
        dico_paquets[nom_joueur]=[]
        mnq= 4-len(main)
        shuffle(dico_defausses[nom_joueur])
        dico_paquets[nom_joueur].extend(dico_defausses[nom_joueur])
        dico_defausses[nom_joueur].clear()
        for i in range(mnq):
            x=dico_paquets[nom_joueur][i]
            main.append(x)
            dico_paquets[nom_joueur].remove(x)
    return main
def selection_carte_main(nom_joueur,main_joueur,choix,dico_defausses):
    for i in main_joueur:
        if i!=choix:
            dico_defausses[nom_joueur].append(i)
def choix_deplacement(nom_joueur,dico_paquets,dico_defausses,configurations,affichage=False):
    main=pioche_main_joueur(nom_joueur,dico_paquets,dico_defausses)
    if configurations[nom_joueur]=="A":
        choix=choix_carte_alea(main)
        selection_carte_main(nom_joueur,main,choix,dico_defausses)
        if affichage==True:
            print("Le paquet du joueur",nom_joueur ,"contient:",dico_paquets[nom_joueur])
            print("La defausse du joueur",nom_joueur,"contient:",dico_defausses[nom_joueur])
            print("Carte déplacement choisie :",choix)
        
    elif configurations[nom_joueur]=="M":
        choix=choix_carte_manuel(nom_joueur,main)
        selection_carte_main(nom_joueur,main,choix,dico_defausses)
        if affichage==True:
            print("Le paquet du joueur",nom_joueur ,"contient:",dico_paquets[nom_joueur])
            print("La defausse du joueur",nom_joueur,"contient:",dico_defausses[nom_joueur])
            print("Carte déplacement choisie :",choix)
    return choix
