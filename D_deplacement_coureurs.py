from C_gestion_cartes import *
from B_initialisation_course import *

######### SECTION : DEPLACEMENT DES COUREURS #################
##############################################################

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus loin
#du start dans la course.
#Elle prend en entrée le dictionnaire des positions
def le_plus_eloigne(dico_positions):
    max_case = -1
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    max_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case > max_case:
            max_joueur = joueur
            max_case = case
    return max_joueur

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus
#en retrait dans la course.
#Elle prend en entrée le dictionnaire des positions et la liste des positions droite
def le_moins_eloigne(dico_positions,positions_droite):
    min_case = len(positions_droite)
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    min_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case < min_case:
            min_joueur = joueur
            min_case = case
    return min_joueur

#A chaque tour, le premier coureur à se déplacer est celui qui est en tête de
#la course, c'est-à-dire situé sur la plus haute case possible et à droite
#Cette fonction renvoie la liste des coureurs par ordre de placement sur la course
#Le coureur le plus proche de l'arrivée est situé en premier
def tri_joueurs(dico_positions):
    copie_dico = dict(dico_positions)
    ordre_joueurs = []
    while len(copie_dico) > 0:
        prochain_joueur = le_plus_eloigne(copie_dico)
        ordre_joueurs.append(prochain_joueur)
        del copie_dico[prochain_joueur]
    return ordre_joueurs
    
def avancee_coureurs(nom_joueur,dico_positions,positions_droite,positions_gauche,distance):
    place=dico_positions[nom_joueur]
    if place[0]=='D':
        position=int(place[1:])
        nvpos=position+distance
        while nvpos >=len(positions_droite):
            nvpos-=1
        while positions_droite[nvpos]!="__":
            nvpos-=1
        positions_droite[position]="__"
        positions_droite[nvpos]=nom_joueur
        dico_positions[nom_joueur]="D"+str(nvpos)
    else:
        position=int(place[1:])
        nvpos=position+distance
        while nvpos>=len(positions_gauche):
                nvpos-=1
        while positions_gauche[nvpos]!="__":
            nvpos-=1
        positions_gauche[position]="__"
        positions_gauche[nvpos]=nom_joueur
        dico_positions[nom_joueur]="G"+str(nvpos)
        
        
def premiere_case_apres_arrivee(nb_lignes,taille_seg,terminus):
    return taille_seg*(nb_lignes - 1) +terminus
    
def test_victoire(nb_lignes,taille_seg,dico_positions,terminus):
    ariv=premiere_case_apres_arrivee(nb_lignes,taille_seg,terminus)
    for i in dico_positions:
        pos=int(dico_positions[i][1:])
        if pos >=ariv:
            return True
    return False
    
def tour_de_jeu_sans_effet(dico_positions,positions_droite,positions_gauche,dico_paquets,dico_defausses,configurations,affichage=False):
    dc={}
    dic=dict(dico_positions)
    for i in dico_positions:
        c=choix_deplacement(i,dico_paquets,dico_defausses,configurations,affichage=False)
        dc[i]=c
    classe=tri_joueurs(dico_positions)
    for i in classe:
        avancee_coureurs(i,dico_positions,positions_droite,positions_gauche,dc[i])
        if affichage==True:
            print("#### Début du tour ####")
            for i in dico_positions:
                x=int(dico_positions[i][1:])-int(dic[i][1:])
                print("Le joueur",i,"se déplace de",x)
            print("#### Fin du tour ####")
    
    
