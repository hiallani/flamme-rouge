from D_deplacement_coureurs import *

######### SECTION : PHASE TERMINALE DU JEU #################
##############################################################

#Fonction distribution_fatigue



#Cette fonction détermine les différents groupes de coureurs,
#qui sont séparés au moins par une case complètement vide
#Elle renvoie donc une liste de listes, dont le premier élément
#est le groupe étant le plus proche de la ligne de départ
# (donc le plus en retard dans la course)
#Dans chaque groupe, les coureurs sont aussi classés du plus en retard
#au plus proche de la ligne d'arrivée
def groupes_coureurs(dico_positions,positions_droite,positions_gauche):
    joueur_min = le_moins_eloigne(dico_positions,positions_droite)
    joueur_max = le_plus_eloigne(dico_positions)
    case_min = numero_de_case(joueur_min,dico_positions)
    case_max = numero_de_case(joueur_max,dico_positions)
    i = case_min
    liste_groupes = []
    while i <= case_max:
        groupe = []
        droite_i = positions_droite[i]
        gauche_i = positions_gauche[i]
        while droite_i != "__":
            if gauche_i != "__":
                groupe.append(gauche_i)
            groupe.append(droite_i)
            i += 1
            droite_i = positions_droite[i]
            gauche_i = positions_gauche[i]
        if len(groupe) > 0:
            liste_groupes.append(groupe)
        i += 1
    return liste_groupes
def decale_coureurs(groupe,dico_positions,positions_droite,positions_gauche):
    if len(groupe)>1:
        groupe.reverse()
        for joueur in groupe:
            avancee_coureurs(joueur,dico_positions,positions_droite,positions_gauche,1)
    else:
        for joueur in groupe:
            avancee_coureurs(joueur,dico_positions,positions_droite,positions_gauche,1)
def aspiration(dico_positions,positions_droite,positions_gauche):
    groupes = groupes_coureurs(dico_positions, positions_droite, positions_gauche)
    i=0
    while i<=len(groupes)-2:
        joueurg1=groupes[i][-1]
        joueurg2=groupes[i+1][0]
        positionj1=int(dico_positions[joueurg1][1:])
        positionj2=int(dico_positions[joueurg2][1:])
        if positionj2==positionj1+2:
            decale_coureurs(groupes[i],dico_positions,positions_droite,positions_gauche)
            groupes=groupes_coureurs(dico_positions,positions_droite,positions_gauche)
        i+=1
def tour_de_jeu(dico_positions,positions_droite,positions_gauche,dico_paquets,dico_defausses,configurations,affichage=False):
    tour_de_jeu_sans_effet(dico_positions,positions_droite,positions_gauche,dico_paquets,dico_defausses,configurations,affichage)
    distribution_fatigue(dico_positions,positions_droite,dico_defausses)
    aspiration(dico_positions,positions_droite,positions_gauche)
