import random*

# Définition des cartes de compétence
cartes_de_competence = {
    'Sprint': {'deplacement': 2, 'description': 'Avancez de 2 cases supplémentaires.'},
    'Esquive': {'deplacement': 0, 'description': 'Annule tout effet négatif ce tour.'},
    'Aspiration': {'deplacement': 1, 'description': 'Avancez d\'une case supplémentaire si vous terminez juste derrière un autre coureur.'},
    'Endurance': {'deplacement': 0, 'description': 'Aucune carte de fatigue tirée ce tour.'},
    'Attaque d\'Équipe': {'deplacement': 2, 'description': 'Si jouée successivement par des coéquipiers, chaque joueur avance de 2 cases supplémentaires.'}
}

# Positions initiales des joueurs
positions_des_joueurs = [0, 0]  # On suppose deux joueurs commençant à la position 0

# Utiliser une carte de compétence
def utiliser_carte_de_competence(nom_de_carte, indice_du_joueur):
    carte = cartes_de_competence[nom_de_carte]
    print(f"Joueur {indice_du_joueur+1} utilise la carte {nom_de_carte}. {carte['description']}")

    # Appliquer l'effet de la carte
    if nom_de_carte == 'Attaque d\'Équipe':
        # Logique supplémentaire pour l'attaque d'équipe
        # Supposons qu'un autre joueur doit jouer la même carte immédiatement après
        if input(f"Le coéquipier a-t-il joué 'Attaque d\'Équipe' juste après le joueur {indice_du_joueur+1}? (oui/non): ").lower() == 'oui':
            print("Attaque d'équipe réussie. Les deux joueurs avancent de 2 cases supplémentaires.")
            positions_des_joueurs[indice_du_joueur] += carte['deplacement']
            positions_des_joueurs[1 - indice_du_joueur] += carte['deplacement']  # En supposant deux joueurs
        else:
            print("Attaque d'équipe échouée. Aucun mouvement supplémentaire.")
    else:
        positions_des_joueurs[indice_du_joueur] += carte['deplacement']


